package com.binar.challenge4.service;

import com.binar.challenge4.model.Users;
import com.binar.challenge4.model.request.UsersRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class UsersServiceImplTest {
    @Autowired
    UsersServiceImpl usersService;

    @Test
    void tesAddData(){
        UsersRequest user = new UsersRequest();
        user.setUsername("Rajab");
        user.setAddress("Indonesia");
        user.setEmail("rajab@gmail.com");
        user.setPassword("rajab");

        usersService.newUser(user);
    }

    @Test
    void tesUpdateData(){
        try{
            UsersRequest user = new UsersRequest();
            user.setIdUser(4);
            user.setUsername("D Rajab");
            user.setAddress("Indonesia");
            user.setEmail("rajab@gmail.com");
            user.setPassword("rajab");

            usersService.updateUser(user);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void deleteUsers(){
        try {
            usersService.deleteUser(1);
        } catch (Exception e) {
            System.out.println("error");
        }
    }

    @Test
    void tesGetUserById() {
        try {
            Users usersList = usersService.searchUserById(1);
            System.out.println("UserName : " + usersList.getUsername());
            System.out.println("Address : " + usersList.getAddress());
            System.out.println("Email : " + usersList.getEmail());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void tesGetUserByName() {
        try {
            List<Users> usersList = usersService.searchUserByName("Rajab");
            usersList.forEach(usr -> {
                System.out.println("Id : " + usr.getIdUser());
            });
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    @Test
    void testGetAllUsers() {
        List<Users> usersList0 = usersService.findAllUser();
        usersList0.forEach(usr -> {
            System.out.println(usr.getUsername());
        });
    }

}