package com.binar.challenge4.service.Interface;

import com.binar.challenge4.model.Users;
import com.binar.challenge4.model.request.UsersRequest;
import com.binar.challenge4.model.response.UsersResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UsersService {

    UsersResponse newUser(UsersRequest user);

    UsersResponse updateUser(UsersRequest user) throws Exception;

    void deleteUser(Integer id_user);

    List<Users> findAllUser();

    Users searchUserById(Integer id_user) throws Exception;

    List<Users> searchUserByName(String username) throws Exception;
}
