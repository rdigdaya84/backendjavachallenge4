package com.binar.challenge4.service.Interface;

import com.binar.challenge4.model.Schedules;
import com.binar.challenge4.model.request.SchedulesRequest;
import com.binar.challenge4.model.response.ScheduleResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ScheduleService {
    ScheduleResponse addSchedule(SchedulesRequest schedules);

    ScheduleResponse updateFilm(SchedulesRequest schedules);

    void deleteSchedules(int idSchedule);

    List<Schedules> findAll();

    List<Schedules> findByFilmId(int idFilm);

}
