package com.binar.challenge4.service;

import com.binar.challenge4.model.Films;
import com.binar.challenge4.model.request.FilmUpdateRequest;
import com.binar.challenge4.model.response.FilmScheduleResponse;
import com.binar.challenge4.repository.FilmRepository;
import com.binar.challenge4.service.Interface.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class FilmServiceImpl implements FilmService {
    @Autowired
    FilmRepository filmRepository;

    @Override
    public void newFilm(Films film) {
        filmRepository.save(film);
    }

    @Override
    public void deleteFilm(Films film) {
        filmRepository.deleteById(film.getIdFilm());
    }

    @Override
    public Films save(Films films) {
        return filmRepository.save(films);
    }

    @Override
    public FilmUpdateRequest updateFilm(FilmUpdateRequest filmUpdateRequest) throws Exception {

        Films film = filmRepository.findFilmsById(filmUpdateRequest.getIdFilm());
        if (film == null) {
            throw new Exception("Data tidak ditemukan");
        }

        try {
            return filmRepository.update(
                    filmUpdateRequest.getFilmCode(),
                    filmUpdateRequest.getFilmName(),
                    filmUpdateRequest.getIsShow(),
                    filmUpdateRequest.getIdFilm());
        } catch (Exception e) {
            e.getMessage();
        }
        return filmUpdateRequest;
    }

    @Override
    public void deleteById(int idFilm) {
        filmRepository.deleteById(idFilm);
    }

    @Override
    public List<Films> findAll() {
        return filmRepository.findAll();
    }

    @Override
    public List<Films> findFilmsShow() throws Exception {
        List<Films> filmResponse = filmRepository.findFilmsShow();
        if (filmResponse == null) {
            throw new Exception("Data tidak ditemukan");
        }
        return filmResponse;
    }

    @Override
    public List<FilmScheduleResponse> findFilmsScheduleByName(String filmName) throws Exception {
        List<FilmScheduleResponse> filmResponse = filmRepository.findFilmsScheduleByName(filmName);
        if (filmResponse == null || filmResponse.size() == 0) {
            throw new Exception("Data tidak ditemukan");
        }
        return filmResponse;
    }

    @Override
    public Films findFilmsById(Integer idFilm) throws Exception {
        Films filmResponse = filmRepository.findFilmsById(idFilm);
        if (filmResponse == null) {
            throw new Exception("Data tidak ditemukan");
        }
        return filmResponse;
    }


}
