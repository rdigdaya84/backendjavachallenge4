package com.binar.challenge4.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SchedulesRequest {
    private Integer idSchedule;
    private String showDate;
    private String startingHour;
    private String endingHour;
    private String ticketPrice;
    private Integer codeFilm;
}
