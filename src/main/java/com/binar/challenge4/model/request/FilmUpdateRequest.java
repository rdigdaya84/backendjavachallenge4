package com.binar.challenge4.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.lang.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class FilmUpdateRequest {
    private Integer idFilm;
    private String filmCode;
    private String filmName;
    private String isShow;
}
