package com.binar.challenge4.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ScheduleResponse {

    private Integer idSchedule;

    private String showDate;

    private String startingHour;

    private String endingHour;

    private String ticketPrice;

    private Integer fsFk;
}
